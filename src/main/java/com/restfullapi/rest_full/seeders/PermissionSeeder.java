package com.restfullapi.rest_full.seeders;

import com.restfullapi.rest_full.enums.Permissions;
import com.restfullapi.rest_full.models.Permission;
import com.restfullapi.rest_full.repositories.PermissionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class PermissionSeeder implements CommandLineRunner {
    private final PermissionRepository permissionRepository;

    @Override
    public void run(String... args) throws Exception {
        List<String> permissions = Arrays.stream(Permissions.values()).map(String::valueOf).toList();
        for (String per : permissions) {
            if (permissionRepository.findByPermissionName(per) != null) {
                continue;
            }
            Permission permission = new Permission();
            permission.setPermissionName(per);
            permission.setDescription("");
            permissionRepository.save(permission);
        }
    }
}

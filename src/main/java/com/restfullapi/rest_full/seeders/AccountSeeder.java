package com.restfullapi.rest_full.seeders;

import com.restfullapi.rest_full.enums.Roles;
import com.restfullapi.rest_full.models.Account;
import com.restfullapi.rest_full.models.Role;
import com.restfullapi.rest_full.models.User;
import com.restfullapi.rest_full.repositories.AccountRepository;
import com.restfullapi.rest_full.repositories.RoleRepository;
import com.restfullapi.rest_full.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class AccountSeeder implements CommandLineRunner, Ordered {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    private final AccountRepository accountRepository;

    @Override
    public void run(String... args) throws Exception {
        Account account = accountRepository.findByUsername("vutuankiet24112002@gmail.com").orElse(null);
        if (account != null) {
            return;
        }
        account = new Account();
        Role role = roleRepository.findByRoleName(String.valueOf(Roles.ADMIN));
        account.setRole(role);
        account.setUsername("vutuankiet24112002@gmail.com");
        account.setPhone("0376658437");
        account.setPassword(new BCryptPasswordEncoder().encode("admin"));
        User user = new User();
        user.setFullName("Administractor");
        user.setPhone("0376658437");
        user.setPhong_ban("Phong1");
        user.setBirthday(new Date());
        user.setEmail("admin@gmail.com");
        account.setUser(userRepository.save(user));
        accountRepository.save(account);
    }

    @Override
    public int getOrder() {
        return 1;
    }
}

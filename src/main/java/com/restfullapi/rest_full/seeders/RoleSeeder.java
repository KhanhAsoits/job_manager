package com.restfullapi.rest_full.seeders;

import com.restfullapi.rest_full.enums.Roles;
import com.restfullapi.rest_full.models.Role;
import com.restfullapi.rest_full.repositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
@RequiredArgsConstructor
public class RoleSeeder implements CommandLineRunner, Ordered {
    private final RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        List<String> roles = Arrays.stream(Roles.values()).map(String::valueOf).toList();
        for (String role : roles) {
            if (roleRepository.findByRoleName(role) != null) {
                continue;
            }
            Role new_role = new Role();
            new_role.setRoleName(role);
            new_role.setDescription("");
            roleRepository.save(new_role);
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }
}

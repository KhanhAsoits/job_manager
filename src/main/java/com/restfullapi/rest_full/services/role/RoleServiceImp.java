package com.restfullapi.rest_full.services.role;

import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.exceptions.NotFoundException;
import com.restfullapi.rest_full.exceptions.PermissionNotFoundException;
import com.restfullapi.rest_full.exceptions.RoleNotFoundException;
import com.restfullapi.rest_full.models.Role;
import com.restfullapi.rest_full.models.Role_;
import com.restfullapi.rest_full.repositories.PermissionRepository;
import com.restfullapi.rest_full.repositories.RoleRepository;
import com.restfullapi.rest_full.requests.role.GivePermissionForRoleRequest;
import com.restfullapi.rest_full.requests.role.RoleRequest;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Transactional
@Service
public class RoleServiceImp implements RoleService {
    private final RoleRepository roleRepository;
    private final PermissionRepository permissionRepository;

    @Override
    public ResponseEntity<?> s_getAll(Pageable pageable) {
        return ApiResponseHelper.gI().resp(
                roleRepository.findAll(pageable),
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_store(RoleRequest roleRequest) {
        String getRoleName = roleRequest.getRoleName().trim();
        boolean roleNameWasExisted = roleRepository.valueWasExisted(Role_.ROLE_NAME, getRoleName, Role.class);

        if (roleNameWasExisted) {
            return ApiResponseHelper.gI().inValid(Message.VALUE_EXISTED);
        }

        return saveRole(roleRequest, null);
    }

    @Override
    public ResponseEntity<?> s_update(Long id, RoleRequest roleRequest) throws Exception {
        Role getRoleById = roleRepository.findById(id)
                .orElseThrow(() -> new RoleNotFoundException(String.format(Message.ROLE_NOT_FOUND, id)));

        String getRoleName = roleRequest.getRoleName().trim();
        String getOldRoleName = getRoleById.getRoleName();
        boolean roleNameWasExisted = roleRepository.valueWasExisted(Role_.ROLE_NAME, getRoleName, Role.class);

        if (!getRoleName.equalsIgnoreCase(getOldRoleName) && roleNameWasExisted) {
            return ApiResponseHelper.gI().inValid(Message.VALUE_EXISTED);
        }
        return saveRole(roleRequest, id);
    }

    private ResponseEntity<?> saveRole(RoleRequest roleRequest, Long id) {
        Role role = new ModelMapper().map(roleRequest, Role.class);
        role.setId(id);

        return ApiResponseHelper.gI().resp(
                roleRepository.save(role),
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE);
    }

    @Override
    public ResponseEntity<?> s_delete(Long id) throws Exception {
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new RoleNotFoundException(String.format(Message.ROLE_NOT_FOUND, id)));

        roleRepository.deleteById(id);

        return ApiResponseHelper.gI().resp(null, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }

    @Override
    public ResponseEntity<?> s_getById(Long id) throws Exception {
        Role role = roleRepository.findById(id)
                .orElseThrow(() -> new RoleNotFoundException(String.format(Message.ROLE_NOT_FOUND, id)));

        return ApiResponseHelper.gI().resp(role, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }

    @Override
    public ResponseEntity<?> s_givePermissionForRole(GivePermissionForRoleRequest givePermissionForRoleRequest,
                                                     Long roleId) throws NotFoundException {
        Role role = roleRepository.findById(roleId)
                .orElseThrow(() -> new RoleNotFoundException(String.format(Message.ROLE_NOT_FOUND, roleId)));
        List<Long> getPermissionIds = givePermissionForRoleRequest.getPermissionIds();

        for (Long id : getPermissionIds) {
            if (!role.exitsPermissionById(id)) {
                role.addPerm(permissionRepository.findById(id)
                        .orElseThrow(() -> new PermissionNotFoundException(String.format(Message.PERMISSION_NOT_FOUND, id))));
            }
        }

        return ApiResponseHelper.gI().resp(roleRepository.saveAndFlush(role), HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }

    @Override
    public ResponseEntity<?> s_removePermissionForRole(GivePermissionForRoleRequest givePermissionForRoleRequest,
                                                       Long roleId) throws Exception {
        Role role = roleRepository.findById(roleId)
                .orElseThrow(() -> new RoleNotFoundException(String.format(Message.ROLE_NOT_FOUND, roleId)));
        List<Long> getPermissionIds = givePermissionForRoleRequest.getPermissionIds();

        for (Long id : getPermissionIds) {
            if (role.exitsPermissionById(id)) {
                role.removePerm(permissionRepository.findById(id)
                        .orElseThrow(() -> new PermissionNotFoundException(String.format(Message.PERMISSION_NOT_FOUND, id))));
            }
        }

        return ApiResponseHelper.gI().resp(roleRepository.saveAndFlush(role), HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }
}

package com.restfullapi.rest_full.services.role;

import com.restfullapi.rest_full.requests.role.GivePermissionForRoleRequest;
import com.restfullapi.rest_full.requests.role.RoleRequest;
import com.restfullapi.rest_full.services.interfaces.ICrudService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface RoleService extends ICrudService<RoleRequest, Long> {
    ResponseEntity<?> s_givePermissionForRole(GivePermissionForRoleRequest givePermissionForRoleRequest, Long roleId) throws Exception;

    ResponseEntity<?> s_removePermissionForRole(GivePermissionForRoleRequest givePermissionForRoleRequest, Long roleId) throws Exception;

}

package com.restfullapi.rest_full.services.auth;

import com.restfullapi.rest_full.requests.ChangePasswordByUserNameRequest;
import com.restfullapi.rest_full.requests.LoginRequest;
import com.restfullapi.rest_full.requests.RegisterRequest;
import com.restfullapi.rest_full.responses.AccountResponse;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public interface AuthService {
    AccountResponse register(RegisterRequest registerRequest) throws Exception;

    AccountResponse login(LoginRequest loginRequest) throws Exception;

    AccountResponse changePasswordByUserName(ChangePasswordByUserNameRequest changePasswordRequest) throws Exception;

    void requestPasswordReset(String email);

    void confirmPasswordReset(String email, String code, String newPassword);

    List<AccountResponse> getAllAccount();
}


package com.restfullapi.rest_full.services.auth;

import com.restfullapi.rest_full.configs.JWTTokenProvider;
import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.enums.Roles;
import com.restfullapi.rest_full.exceptions.EmailNotFoundException;
import com.restfullapi.rest_full.exceptions.ExpiredResetCodeException;
import com.restfullapi.rest_full.exceptions.InvalidResetCodeException;
import com.restfullapi.rest_full.exceptions.PasswordIncorrectException;
import com.restfullapi.rest_full.logic.CodeGenerator;
import com.restfullapi.rest_full.models.*;
import com.restfullapi.rest_full.repositories.AccountRepository;
import com.restfullapi.rest_full.repositories.RoleRepository;
import com.restfullapi.rest_full.repositories.UserRepository;
import com.restfullapi.rest_full.requests.ChangePasswordByUserNameRequest;
import com.restfullapi.rest_full.requests.LoginRequest;
import com.restfullapi.rest_full.requests.RegisterRequest;
import com.restfullapi.rest_full.responses.AccountResponse;
import com.restfullapi.rest_full.services.email.MailService;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Transactional
@Service
@RequiredArgsConstructor
public class AuthServiceImp implements AuthService {

    private final JWTTokenProvider jwtTokenProvider;
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final MailService mailService;

    @Override
    public AccountResponse register(RegisterRequest registerRequest) throws Exception {
        Account account = new Account();
        Role role = roleRepository.findByRoleName(registerRequest.getRoleName());
        if (role == null) {
            role = roleRepository.findByRoleName(String.valueOf(Roles.STAFF));
        }
        User user = new User();

        user.setPhone(registerRequest.getPhone());
        user.setEmail(registerRequest.getEmail());
        user.setFullName(registerRequest.getFullName());
        userRepository.save(user);

        account.setUsername(registerRequest.getUsername());
        account.setStatus(1);
        account.setPassword(new BCryptPasswordEncoder().encode(registerRequest.getPassword()));
        account.setRole(role);
        account.setUser(user);

        accountRepository.save(account);
        AccountResponse accountResponse = new ModelMapper().map(account, AccountResponse.class);
        return accountResponse;
    }

    @Override
    public AccountResponse login(LoginRequest loginRequest) throws Exception {
        Account account = accountRepository.findByUsername(loginRequest.getUsername()).orElseThrow(() -> new UsernameNotFoundException("No user found"));
        if (new BCryptPasswordEncoder().matches(loginRequest.getPassword(), account.getPassword())) {
            AccountResponse accountResponse = new ModelMapper().map(account, AccountResponse.class);
            String token = jwtTokenProvider.generateTokenFromUsername(loginRequest.getUsername());
            accountResponse.setToken(token);
            return accountResponse;
        } else {
            throw new PasswordIncorrectException("Password incorrect!");
        }
    }

    @Override
    public AccountResponse changePasswordByUserName(ChangePasswordByUserNameRequest changePasswordRequest) throws Exception {
        String userName = changePasswordRequest.getUserName();
        String oldPassword = changePasswordRequest.getOldPassword();
        String newPassword = changePasswordRequest.getNewPassword();
        Optional<Account> optionalAccount = Optional.ofNullable(accountRepository.findByUsername(userName).orElseThrow(() -> new UsernameNotFoundException("No user found")));
        Account account = optionalAccount.get();
        if (new BCryptPasswordEncoder().matches(oldPassword, account.getPassword())) {
            account.setPassword(new BCryptPasswordEncoder().encode(newPassword));
            accountRepository.save(account);
            return ApiResponseHelper.gI().createAccountResponse(account, Message.DEFAULT_SUCCESS_MESSAGE);
        } else {
            throw new PasswordIncorrectException(Message.PASSWORD_INCORRECT);
        }
    }

    @Override
    public void requestPasswordReset(String email) {
        Optional<Account> optionalAccount = accountRepository.findByUsername(email);
        Account account = null;
        try {
            account = optionalAccount.orElseThrow(() -> new EmailNotFoundException("No user found"));
        } catch (EmailNotFoundException e) {
            throw new RuntimeException(e);
        }
        CodeGenerator codeGenerator = new CodeGenerator();
        String resetCode = codeGenerator.generateRandomCode();
        int time = 5;
        Timestamp expiryTime = Timestamp.valueOf(LocalDateTime.now().plusMinutes(time));

        account.setResetCode(resetCode);
        account.setResetCodeExpiry(expiryTime);
        accountRepository.save(account);
        mailService.sendResetCodeByEmail(email, resetCode, time);
    }

    @Override
    public void confirmPasswordReset(String email, String code, String newPassword) {
        Optional<Account> optionalAccount = accountRepository.findByUsername(email);
        Account account = null;
        try {
            account = optionalAccount.orElseThrow(() -> new EmailNotFoundException("No user found"));
        } catch (EmailNotFoundException e) {
            throw new RuntimeException(e);
        }

        if (!code.equals(account.getResetCode())) {
            try {
                throw new InvalidResetCodeException(Message.INVALID_RESET_CODE);
            } catch (InvalidResetCodeException e) {
                throw new RuntimeException(e);
            }
        }

        if (LocalDateTime.now().isAfter(account.getResetCodeExpiry().toLocalDateTime())) {
            try {
                throw new ExpiredResetCodeException(Message.EXPIRED_RESET_CODE);
            } catch (ExpiredResetCodeException e) {
                throw new RuntimeException(e);
            }
        }
        account.setPassword(new BCryptPasswordEncoder().encode(newPassword));
        account.setResetCode(null);
        account.setResetCodeExpiry(null);
        accountRepository.save(account);
    }

    @Override
    public List<AccountResponse> getAllAccount() {
        Role role = roleRepository.findByRoleName(String.valueOf(Roles.ADMIN));
        List<Account> accounts = accountRepository.findAllIfNotRole(role.getId());

        return accounts.stream().map(ac -> new ModelMapper().map(ac, AccountResponse.class)).collect(Collectors.toList());
    }
}

package com.restfullapi.rest_full.services.kpi;

import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.exceptions.NotFoundException;
import com.restfullapi.rest_full.models.*;
import com.restfullapi.rest_full.repositories.JobRepository;
import com.restfullapi.rest_full.repositories.KPICategoryRepository;
import com.restfullapi.rest_full.repositories.KpiRepository;
import com.restfullapi.rest_full.repositories.UserRepository;
import com.restfullapi.rest_full.requests.kpi.KpiRequest;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Transactional
@RequiredArgsConstructor
@Service
public class KpiServiceImp implements KpiService {
    private final KpiRepository kpiRepository;
    private final KPICategoryRepository kpiCategoryRepository;
    private final JobRepository jobRepository;
    private final UserRepository userRepository;

    @Override
    public ResponseEntity<?> s_getAll(Pageable pageable) throws Exception {
        return ApiResponseHelper.gI().resp(
                kpiRepository.findAll(pageable),
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_store(KpiRequest kpiRequest) throws Exception {
        boolean kpiNameWasExisted =
                kpiRepository.valueWasExisted(Kpi_.KPI_NAME, kpiRequest.getKpiName(), Kpi.class);

        if (kpiNameWasExisted) {
            return ApiResponseHelper.gI().inValid(Message.VALUE_EXISTED);
        }

        return saveKpi(handleKpiRequest(kpiRequest));
    }

    @Override
    public ResponseEntity<?> s_update(String id, KpiRequest kpiRequest) throws Exception {
        Kpi getKpiById = kpiRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_NOT_FOUND, id)));

        String getOldKpiName = getKpiById.getKpiName();
        String getNewKpiName = kpiRequest.getKpiName();
        boolean kpiNameWasExisted =
                kpiRepository.valueWasExisted(Kpi_.KPI_NAME, getNewKpiName, Kpi.class);

        if (!getOldKpiName.equalsIgnoreCase(getNewKpiName) && kpiNameWasExisted) {
            return ApiResponseHelper.gI().inValid(Message.VALUE_EXISTED);
        }

        Kpi kpi = handleKpiRequest(kpiRequest);
        kpi.setId(id);

        return saveKpi(kpi);
    }

    private Kpi handleKpiRequest(KpiRequest kpiRequest) throws NotFoundException {
        Long kpiCategoryId = (long) kpiRequest.getKpiTypeId();
        KPICategory getKpiCategoryById = kpiCategoryRepository.findById(kpiCategoryId)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_CATEGORY_NOT_FOUND, kpiCategoryId)));

        String jobId = kpiRequest.getJobId();
        Job getJobById = jobRepository.findById(jobId)
                .orElseThrow(() -> new NotFoundException(String.format(Message.JOB_NOTE_FOUND, jobId)));

        String userId = kpiRequest.getUserId();
        User getUserById = null;
        if (userId != null) {
            getUserById = userRepository.findById(userId)
                    .orElseThrow(() -> new NotFoundException(Message.USER_NOT_FOUND));
        }

        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);
        Kpi kpi = modelMapper.map(kpiRequest, Kpi.class);
        kpi.setKpiCategory(getKpiCategoryById);
        kpi.setJob(getJobById);
        kpi.setUser(getUserById);

        return kpi;
    }

    private ResponseEntity<?> saveKpi(Kpi kpi) {
        return ApiResponseHelper.gI().resp(
                kpiRepository.saveAndFlush(kpi),
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_delete(String id) throws Exception {
        Kpi getKpiById = kpiRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_NOT_FOUND, id)));

        kpiRepository.deleteById(id);

        return ApiResponseHelper.gI().resp(
                null,
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_getById(String id) throws Exception {
        Kpi getKpiById = kpiRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_NOT_FOUND, id)));

        return ApiResponseHelper.gI().resp(
                getKpiById,
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }
}

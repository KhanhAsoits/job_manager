package com.restfullapi.rest_full.services.kpi;

import com.restfullapi.rest_full.requests.kpi.KpiRequest;
import com.restfullapi.rest_full.services.interfaces.ICrudService;

public interface KpiService extends ICrudService<KpiRequest, String> {
}

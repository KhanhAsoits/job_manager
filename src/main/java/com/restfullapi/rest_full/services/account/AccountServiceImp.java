package com.restfullapi.rest_full.services.account;

import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.enums.Roles;
import com.restfullapi.rest_full.exceptions.NotFoundException;
import com.restfullapi.rest_full.models.Account;
import com.restfullapi.rest_full.models.Role;
import com.restfullapi.rest_full.models.User;
import com.restfullapi.rest_full.repositories.AccountRepository;
import com.restfullapi.rest_full.repositories.RoleRepository;
import com.restfullapi.rest_full.repositories.UserRepository;
import com.restfullapi.rest_full.requests.account.AccountRequest;
import com.restfullapi.rest_full.responses.AccountResponse;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@RequiredArgsConstructor
@Transactional
@Service
public class AccountServiceImp implements AccountService{
    private final AccountRepository accountRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Override
    public ResponseEntity<?> s_getAll(Pageable pageable) throws Exception {
        return ApiResponseHelper.gI().resp(accountRepository.findAll(pageable), HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }

    @Override
    public ResponseEntity<?> s_store(AccountRequest accountRequest) throws Exception {
        try{
            Account account = new Account();
            Role role = roleRepository.findByRoleName(accountRequest.getRoleName());
            if (role == null) {
                role = roleRepository.findByRoleName(String.valueOf(Roles.STAFF));
            }
            User user = new User();

            user.setPhone(accountRequest.getPhone());
            user.setEmail(accountRequest.getEmail());
            user.setPhong_ban(accountRequest.getPhong_ban());
            user.setBirthday(accountRequest.getBirthday());
            user.setFullName(accountRequest.getFullName());
            userRepository.save(user);

            account.setUsername(accountRequest.getUsername());
            account.setStatus(accountRequest.getStatus());
            account.setPhone(accountRequest.getPhone());
            if(accountRequest.getStatus() == null){
                account.setStatus(1);
            }
            account.setPassword(new BCryptPasswordEncoder().encode(accountRequest.getPassword()));
            account.setRole(role);
            account.setUser(user);

            accountRepository.save(account);
            AccountResponse accountResponse = new ModelMapper().map(account, AccountResponse.class);
            return ApiResponseHelper.gI().resp(accountResponse, HttpStatus.CREATED, Message.DEFAULT_SUCCESS_MESSAGE);
        } catch (Exception e) {
            return ApiResponseHelper.gI().resp(null, HttpStatus.INTERNAL_SERVER_ERROR, Message.DEFAULT_ERROR_MESSAGE);
        }
    }

    @Override
    public ResponseEntity<?> s_update(String id, AccountRequest accountRequest) throws Exception {
        try {
            Optional<Account> optionalAccount = accountRepository.findById(Long.parseLong(id));
            if (optionalAccount.isPresent()) {
                Account existingAccount = optionalAccount.get();
                existingAccount.setUsername(accountRequest.getUsername());
                existingAccount.setStatus(accountRequest.getStatus());
                existingAccount.setPhone(accountRequest.getPhone());
                existingAccount.setPassword(new BCryptPasswordEncoder().encode(accountRequest.getPassword()));
                if (accountRequest.getRoleId() != null) {
                    Role role = roleRepository.findById(accountRequest.getRoleId())
                            .orElseThrow(() -> new NotFoundException("Role not found"));
                    existingAccount.setRole(role);
                }
                if (existingAccount.getUser() != null) {
                    User user = existingAccount.getUser();
                    user.setEmail(accountRequest.getEmail());
                    user.setFullName(accountRequest.getFullName());
                    user.setPhone(accountRequest.getPhone());
                    user.setPhong_ban(accountRequest.getPhong_ban());
                    user.setBirthday(accountRequest.getBirthday());
                    userRepository.save(user);
                    existingAccount.setUser(user);
                }
                accountRepository.save(existingAccount);
                AccountResponse accountResponse = new ModelMapper().map(existingAccount, AccountResponse.class);
                return ApiResponseHelper.gI().resp(accountResponse, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
            } else {
                return ApiResponseHelper.gI().resp(null, HttpStatus.NOT_FOUND, Message.ACCOUNT_NOT_FOUND);
            }
        } catch (Exception e) {
            return ApiResponseHelper.gI().resp(null, HttpStatus.INTERNAL_SERVER_ERROR, Message.DEFAULT_ERROR_MESSAGE);
        }
    }

    @Override
    public ResponseEntity<?> s_delete(String id) throws Exception {
        try {
            Optional<Account> optionalAccount = accountRepository.findById(Long.parseLong(id));
            Account account = optionalAccount.orElseThrow(() -> new RuntimeException("Account not found"));
            account.setStatus(0);
            accountRepository.save(account);
            return ApiResponseHelper.gI().resp(null, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
        } catch (Exception e) {
            return ApiResponseHelper.gI().resp(null, HttpStatus.INTERNAL_SERVER_ERROR, Message.DEFAULT_ERROR_MESSAGE);
        }
    }

    @Override
    public ResponseEntity<?> s_getById(String id) throws Exception {
        try {
            Optional<Account> optionalAccount = accountRepository.findById(Long.parseLong(id));
            if (optionalAccount.isPresent()) {
                Account account = optionalAccount.get();
                AccountResponse accountResponse = new ModelMapper().map(account, AccountResponse.class);
                return ApiResponseHelper.gI().resp(accountResponse, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
            } else {
                return ApiResponseHelper.gI().resp(null, HttpStatus.NOT_FOUND, Message.NOT_FOUND);
            }
        } catch (Exception e) {
            return ApiResponseHelper.gI().resp(null, HttpStatus.INTERNAL_SERVER_ERROR, Message.DEFAULT_ERROR_MESSAGE);
        }
    }
}

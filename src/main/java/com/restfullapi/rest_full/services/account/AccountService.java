package com.restfullapi.rest_full.services.account;

import com.restfullapi.rest_full.requests.account.AccountRequest;
import com.restfullapi.rest_full.services.interfaces.ICrudService;
import org.springframework.stereotype.Service;

@Service
public interface AccountService extends ICrudService<AccountRequest,String> {
}

package com.restfullapi.rest_full.services.job;

import com.restfullapi.rest_full.containts.Authenticaion;
import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.exceptions.NotFoundException;
import com.restfullapi.rest_full.models.Job;
import com.restfullapi.rest_full.models.JobDetail;
import com.restfullapi.rest_full.models.User;
import com.restfullapi.rest_full.models.*;
import com.restfullapi.rest_full.repositories.AccountRepository;
import com.restfullapi.rest_full.repositories.JobRepository;
import com.restfullapi.rest_full.repositories.UserRepository;
import com.restfullapi.rest_full.requests.job.JobRequest;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import com.restfullapi.rest_full.ultis.SystemHelper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.sql.Timestamp;

@RequiredArgsConstructor
@Transactional
@Service
public class JobServiceImp implements JobService {
    private final JobRepository jobRepository;
    private final UserRepository userRepository;
    private final AccountRepository accountRepository;

    @Override
    public ResponseEntity<?> s_getAll(Pageable pageable) {
        Account account = accountRepository.findByUsername(SystemHelper.getUsernameOfLoggedUser()).orElseThrow(() -> new UsernameNotFoundException(Message.USER_NOT_FOUND));
        if (Authenticaion.CAN_READ_ALL_JOB_ROLE.contains(account.getRole().getRoleName())) {
            return ApiResponseHelper.gI().resp(jobRepository.findAll(pageable), HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
        }
        return ApiResponseHelper.gI().resp(jobRepository.findAllByUser(pageable, account.getUser().getId()), HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }

    @Override
    public ResponseEntity<?> s_store(JobRequest jobRequest) throws Exception {
        Job job = handleJobRequest(jobRequest);

        return ApiResponseHelper.gI().resp(
                jobRepository.saveAndFlush(job),
                HttpStatus.CREATED,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_update(String id, JobRequest jobRequest) throws Exception {
        if (jobRepository.existsById(id)) {
            Job job = handleJobRequest(jobRequest);
            job.setId(id);
            job.getJobDetail().setId(id);

            return ApiResponseHelper.gI().resp(
                    jobRepository.saveAndFlush(job),
                    HttpStatus.OK,
                    Message.DEFAULT_SUCCESS_MESSAGE
            );
        } else {
            return ApiResponseHelper.gI().resp(null, HttpStatus.NOT_FOUND, Message.NOT_FOUND);
        }
    }

    private Job handleJobRequest(JobRequest jobRequest) throws NotFoundException {
        User staff = userRepository.findById(jobRequest.getStaff())
                .orElseThrow(() -> new NotFoundException("Staff not found"));
        User receiver = userRepository.findById(jobRequest.getReceiver())
                .orElseThrow(() -> new NotFoundException("Receiver not found"));

        Job job = new ModelMapper().map(jobRequest, Job.class);
        job.setStaff(staff);
        job.setReceiver(receiver);

        JobDetail jobDetail = new ModelMapper().map(jobRequest.getJobDetail(), JobDetail.class);
        jobDetail.setJob(job);

        if (jobRequest.getJobDetail().getTimeStart() == null) {
            jobDetail.setTimeStart(new Timestamp(System.currentTimeMillis()));
        }

        job.setJobDetail(jobDetail);

        return job;
    }

    @Override
    public ResponseEntity<?> s_delete(String id) throws Exception {
        Job getJobById = jobRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(Message.JOB_NOTE_FOUND, id)));

        jobRepository.deleteById(id);
        return ApiResponseHelper.gI().resp(null, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }

    @Override
    public ResponseEntity<?> s_getById(String id) throws Exception {
        Job getJobById = jobRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(Message.JOB_NOTE_FOUND, id)));

        return ApiResponseHelper.gI().resp(getJobById, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }
}

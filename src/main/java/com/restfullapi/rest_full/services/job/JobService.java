package com.restfullapi.rest_full.services.job;

import com.restfullapi.rest_full.requests.job.JobRequest;
import com.restfullapi.rest_full.services.interfaces.ICrudService;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public interface JobService extends ICrudService<JobRequest,String> {

}

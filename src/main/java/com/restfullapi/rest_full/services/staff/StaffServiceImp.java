package com.restfullapi.rest_full.services.staff;

import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.enums.Roles;
import com.restfullapi.rest_full.models.Account;
import com.restfullapi.rest_full.models.Role;
import com.restfullapi.rest_full.repositories.AccountRepository;
import com.restfullapi.rest_full.repositories.RoleRepository;
import com.restfullapi.rest_full.requests.account.AccountRequest;
import com.restfullapi.rest_full.responses.AccountResponse;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional
@Service
public class StaffServiceImp implements StaffService{
    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;

    @Override
    public ResponseEntity<?> s_getAll(Pageable pageable) throws Exception {
        try {
            Role role = roleRepository.findByRoleName(String.valueOf(Roles.STAFF));
            List<Account> staffAccounts = accountRepository.findAllByRoleId(role.getId());
            List<AccountResponse> staffResponses = staffAccounts.stream()
                    .map(account -> new ModelMapper().map(account, AccountResponse.class))
                    .collect(Collectors.toList());
            return ApiResponseHelper.gI().resp(staffResponses, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @Override
    public ResponseEntity<?> s_store(AccountRequest accountRequest) throws Exception {
        return null;
    }

    @Override
    public ResponseEntity<?> s_update(String id, AccountRequest accountRequest) throws Exception {
        return null;
    }

    @Override
    public ResponseEntity<?> s_delete(String id) throws Exception {
        return null;
    }

    @Override
    public ResponseEntity<?> s_getById(String id) throws Exception {
        return null;
    }
}

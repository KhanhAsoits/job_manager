package com.restfullapi.rest_full.services.staff;

import com.restfullapi.rest_full.requests.account.AccountRequest;
import com.restfullapi.rest_full.services.interfaces.ICrudService;
import org.springframework.stereotype.Service;

@Service
public interface StaffService extends ICrudService<AccountRequest,String> {
}

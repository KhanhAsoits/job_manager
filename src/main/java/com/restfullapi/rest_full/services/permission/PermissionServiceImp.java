package com.restfullapi.rest_full.services.permission;

import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.models.Permission;
import com.restfullapi.rest_full.repositories.PermissionRepository;
import com.restfullapi.rest_full.requests.permission.PermissionRequest;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
@RequiredArgsConstructor
public class PermissionServiceImp implements PermissionService {
    private final PermissionRepository permissionRepository;

    @Override
    public ResponseEntity<?> s_getAll(Pageable pageable) {
        List<Permission> permissions = permissionRepository.findAll();

        if (permissions.isEmpty()) {
            return ApiResponseHelper.gI().notFound(Message.NO_DATA);
        }

        return ApiResponseHelper.gI().resp(permissions, HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
    }

    @Override
    public ResponseEntity<?> s_store(PermissionRequest permissionRequest) {
        return null;
    }

    @Override
    public ResponseEntity<?> s_update(Long id, PermissionRequest permissionRequest) {
        return null;
    }

    @Override
    public ResponseEntity<?> s_delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<?> s_getById(Long id) {
        return null;
    }
}

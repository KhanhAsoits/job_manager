package com.restfullapi.rest_full.services.permission;

import com.restfullapi.rest_full.requests.permission.PermissionRequest;
import com.restfullapi.rest_full.services.interfaces.ICrudService;
import org.springframework.stereotype.Service;

@Service
public interface PermissionService extends ICrudService<PermissionRequest, Long> {
}

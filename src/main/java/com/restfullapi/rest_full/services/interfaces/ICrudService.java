package com.restfullapi.rest_full.services.interfaces;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

public interface ICrudService<Request, IdType> {
    ResponseEntity<?> s_getAll(Pageable pageable) throws Exception;

    ResponseEntity<?> s_store(Request request) throws Exception;

    ResponseEntity<?> s_update(IdType id, Request request) throws Exception;

    ResponseEntity<?> s_delete(IdType id) throws Exception;

    ResponseEntity<?> s_getById(IdType id) throws Exception;
}

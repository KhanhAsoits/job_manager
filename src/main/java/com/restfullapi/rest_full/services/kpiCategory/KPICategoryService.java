package com.restfullapi.rest_full.services.kpiCategory;

import com.restfullapi.rest_full.requests.kpiCategory.GiveRoleForKPICategoryRequest;
import com.restfullapi.rest_full.requests.kpiCategory.KPICategoryRequest;
import com.restfullapi.rest_full.services.interfaces.ICrudService;
import org.springframework.http.ResponseEntity;

public interface KPICategoryService extends ICrudService<KPICategoryRequest, Long> {
    ResponseEntity<?> s_giveRoleForKPICategory(GiveRoleForKPICategoryRequest request,
                                               Long kpiCategoryId) throws Exception;

    ResponseEntity<?> s_removeRoleForKPICategory(GiveRoleForKPICategoryRequest request,
                                                 Long kpiCategoryId) throws Exception;
}

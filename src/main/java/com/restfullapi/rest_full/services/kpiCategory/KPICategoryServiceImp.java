package com.restfullapi.rest_full.services.kpiCategory;

import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.exceptions.NotFoundException;
import com.restfullapi.rest_full.exceptions.RoleNotFoundException;
import com.restfullapi.rest_full.models.KPICategory;
import com.restfullapi.rest_full.repositories.KPICategoryRepository;
import com.restfullapi.rest_full.repositories.RoleRepository;
import com.restfullapi.rest_full.requests.kpiCategory.GiveRoleForKPICategoryRequest;
import com.restfullapi.rest_full.requests.kpiCategory.KPICategoryRequest;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Transactional
@Service
public class KPICategoryServiceImp implements KPICategoryService {
    private final String KPICategory_NAME = "name";
    private final KPICategoryRepository kpiCategoryRepository;
    private final RoleRepository roleRepository;

    @Override
    public ResponseEntity<?> s_getAll(Pageable pageable) {
        return ApiResponseHelper.gI().resp(
                kpiCategoryRepository.findAll(pageable),
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_store(KPICategoryRequest kpiCategoryRequest) {
        String getKpiCategoryName = kpiCategoryRequest.getName().trim();
        boolean kpiCategoryNameWasExisted =
                kpiCategoryRepository.valueWasExisted(KPICategory_NAME, getKpiCategoryName, KPICategory.class);

        if (kpiCategoryNameWasExisted) {
            return ApiResponseHelper.gI().inValid(Message.VALUE_EXISTED);
        }

        return saveKPICategory(kpiCategoryRequest, null);
    }

    @Override
    public ResponseEntity<?> s_update(Long id, KPICategoryRequest kpiCategoryRequest) throws Exception {
        KPICategory getKpiCategoryById = kpiCategoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_CATEGORY_NOT_FOUND, id)));

        String newKPICategoryName = kpiCategoryRequest.getName().trim();
        String oldKpiCategoryName = getKpiCategoryById.getName();
        boolean newKpiCategoryNameWasExisted =
                kpiCategoryRepository.valueWasExisted(KPICategory_NAME, newKPICategoryName, KPICategory.class);

        if (!oldKpiCategoryName.equalsIgnoreCase(newKPICategoryName)
                && newKpiCategoryNameWasExisted) {
            return ApiResponseHelper.gI().inValid(Message.VALUE_EXISTED);
        }

        return saveKPICategory(kpiCategoryRequest, id);
    }

    private ResponseEntity<?> saveKPICategory(KPICategoryRequest kpiCategoryRequest, Long id) {
        KPICategory kpiCategory = new ModelMapper().map(kpiCategoryRequest, KPICategory.class);
        kpiCategory.setId(id);

        return ApiResponseHelper.gI().resp(
                kpiCategoryRepository.saveAndFlush(kpiCategory),
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_delete(Long id) throws Exception {
        KPICategory getKpiCategoryById = kpiCategoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_CATEGORY_NOT_FOUND, id)));

        kpiCategoryRepository.deleteById(id);

        return ApiResponseHelper.gI().resp(
                null,
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_getById(Long id) throws Exception {
        KPICategory getKpiCategoryById = kpiCategoryRepository.findById(id)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_CATEGORY_NOT_FOUND, id)));

        return ApiResponseHelper.gI().resp(
                getKpiCategoryById,
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_giveRoleForKPICategory(GiveRoleForKPICategoryRequest request,
                                                      Long kpiCategoryId) throws Exception {
        KPICategory kpiCategory = kpiCategoryRepository.findById(kpiCategoryId)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_CATEGORY_NOT_FOUND, kpiCategoryId)));

        List<Long> roleIds = request.getRoleIds();
        for (Long id : roleIds) {
            if (!kpiCategory.roleWasExisted(id)) {
                kpiCategory.addRole(roleRepository.findById(id)
                        .orElseThrow(() -> new RoleNotFoundException(String.format(Message.ROLE_NOT_FOUND, id))));
            }
        }

        return ApiResponseHelper.gI().resp(
                kpiCategoryRepository.saveAndFlush(kpiCategory),
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }

    @Override
    public ResponseEntity<?> s_removeRoleForKPICategory(GiveRoleForKPICategoryRequest request,
                                                        Long kpiCategoryId) throws Exception {
        KPICategory kpiCategory = kpiCategoryRepository.findById(kpiCategoryId)
                .orElseThrow(() -> new NotFoundException(String.format(Message.KPI_CATEGORY_NOT_FOUND, kpiCategoryId)));

        List<Long> roleIds = request.getRoleIds();
        for (Long id : roleIds) {
            if (kpiCategory.roleWasExisted(id)) {
                kpiCategory.removeRole(roleRepository.findById(id)
                        .orElseThrow(() -> new RoleNotFoundException(String.format(Message.ROLE_NOT_FOUND, id))));
            }
        }

        return ApiResponseHelper.gI().resp(
                kpiCategoryRepository.saveAndFlush(kpiCategory),
                HttpStatus.OK,
                Message.DEFAULT_SUCCESS_MESSAGE
        );
    }
}

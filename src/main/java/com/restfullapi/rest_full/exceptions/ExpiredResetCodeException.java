package com.restfullapi.rest_full.exceptions;

public class ExpiredResetCodeException extends Exception{
    public ExpiredResetCodeException(String msg) {
        super(msg);
    }
}

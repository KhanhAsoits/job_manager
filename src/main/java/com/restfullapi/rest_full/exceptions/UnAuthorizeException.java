package com.restfullapi.rest_full.exceptions;

public class UnAuthorizeException extends Exception {
    public UnAuthorizeException(String msg) {
        super(msg);
    }
}

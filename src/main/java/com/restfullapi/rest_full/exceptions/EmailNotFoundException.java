package com.restfullapi.rest_full.exceptions;

public class EmailNotFoundException extends Exception{
    public EmailNotFoundException(String msg) {
        super(msg);
    }
}

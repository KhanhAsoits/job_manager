package com.restfullapi.rest_full.exceptions;

public class PermissionNotFoundException extends NotFoundException {
    public PermissionNotFoundException(String msg) {
        super(msg);
    }
}

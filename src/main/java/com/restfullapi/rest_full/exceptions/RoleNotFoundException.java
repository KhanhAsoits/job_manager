package com.restfullapi.rest_full.exceptions;

public class RoleNotFoundException extends NotFoundException {
    public RoleNotFoundException(String msg) {
        super(msg);
    }
}

package com.restfullapi.rest_full.exceptions;

public class InvalidResetCodeException extends Exception{
    public InvalidResetCodeException(String msg) {
        super(msg);
    }
}

package com.restfullapi.rest_full.exceptions;

import com.restfullapi.rest_full.containts.Message;

public class NoPermissionException extends Exception {
    public NoPermissionException() {
        super(Message.NO_PERMISSION);
    }
}

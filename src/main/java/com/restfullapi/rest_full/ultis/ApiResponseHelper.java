package com.restfullapi.rest_full.ultis;

import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.models.Account;
import com.restfullapi.rest_full.responses.AccountResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ApiResponseHelper {
    private static ApiResponseHelper I;

    public static ApiResponseHelper gI() {
        if (I == null) {
            I = new ApiResponseHelper();
        }
        return I;
    }

    @Data
    @AllArgsConstructor
    private static class ApiResponse {
        private String message;
        private Object response;
    }

    public ResponseEntity<ApiResponse> resp(Object entity, HttpStatus status, String message) {
        return new ResponseEntity<>(new ApiResponse(message, entity), status);
    }

    public ResponseEntity<?> fallback(Exception e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage(), null), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public ResponseEntity<?> noPermission() {
        return new ResponseEntity<>(new ApiResponse(Message.NO_PERMISSION, null), HttpStatus.UNAUTHORIZED);
    }

    public ResponseEntity<?> inValid(String errorMessage) {
        return new ResponseEntity<>(new ApiResponse(errorMessage, null), HttpStatus.BAD_REQUEST);
    }

    public ResponseEntity<?> notFound(String errorMessage) {
        return new ResponseEntity<>(new ApiResponse(errorMessage, null), HttpStatus.BAD_REQUEST);
    }

    public AccountResponse createAccountResponse(Account account, String message) {
        // Your implementation here to create and return an AccountResponse
        return new AccountResponse(/* parameters based on your AccountResponse class */);
    }
}

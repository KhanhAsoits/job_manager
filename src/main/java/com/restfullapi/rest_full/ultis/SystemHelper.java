package com.restfullapi.rest_full.ultis;

import com.restfullapi.rest_full.models.CustomUserDetail;
import org.springframework.security.core.context.SecurityContextHolder;

public class SystemHelper {
    public static CustomUserDetail getCurrentLoggedUser() {
        return (CustomUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static String getUsernameOfLoggedUser() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }
}

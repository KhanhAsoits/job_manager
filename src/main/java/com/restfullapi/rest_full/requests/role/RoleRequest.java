package com.restfullapi.rest_full.requests.role;

import com.restfullapi.rest_full.containts.Message;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleRequest {
    @NotBlank(message = Message.REQUIRED)
    private String roleName;
    private String description;
    private List<Long> permissionIds;
    private List<Long> kpiCategoryIds;
}

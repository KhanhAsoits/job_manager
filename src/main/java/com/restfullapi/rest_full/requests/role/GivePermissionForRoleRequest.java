package com.restfullapi.rest_full.requests.role;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class GivePermissionForRoleRequest {
    @NotNull
    private List<Long> permissionIds = new ArrayList<>();
}

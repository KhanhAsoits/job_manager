package com.restfullapi.rest_full.requests.account;

import jakarta.persistence.Column;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.sql.Date;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
public class AccountRequest {
    @NotBlank(message = "Username required")
    private String username;
    @NotBlank(message = "Password required")
    private String password;

    @Email(message = "Email invalid")
    @NotBlank(message = "Email required")
    private String email;

    @NotBlank(message = "Fullname required")
    private String fullName;

    @Length(max = 12, min = 10)
    private String phone;

    @NotNull
    private String phong_ban;

    private Date birthday;

    private String managerId;

    @NotNull
    private Long roleId;

    private Long status;

    private String roleName;
}

package com.restfullapi.rest_full.requests.kpi;

import com.restfullapi.rest_full.containts.Message;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KpiRequest {
    @NotBlank(message = Message.REQUIRED)
    @Length(max = 255, message = Message.CANT_EXCEED_255_CHARACTERS)
    private String kpiName;

    @Length(max = 1000, message = Message.CANT_EXCEED_1000_CHARACTERS)
    private String description;

    @NotNull(message = Message.REQUIRED)
    private int target;

    @NotNull(message = Message.REQUIRED)
    private int kpiTypeId;

    @NotBlank(message = Message.REQUIRED)
    private String jobId;

    private String userId;
}

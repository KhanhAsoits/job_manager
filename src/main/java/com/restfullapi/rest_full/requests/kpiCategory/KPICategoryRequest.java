package com.restfullapi.rest_full.requests.kpiCategory;

import com.restfullapi.rest_full.containts.Message;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KPICategoryRequest {
    @NotBlank(message = Message.REQUIRED)
    private String name;
    private String description;
}

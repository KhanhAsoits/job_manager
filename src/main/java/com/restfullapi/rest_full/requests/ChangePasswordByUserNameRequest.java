package com.restfullapi.rest_full.requests;

import lombok.Data;

@Data
public class ChangePasswordByUserNameRequest {
    private String userName;
    private String oldPassword;
    private String newPassword;
}
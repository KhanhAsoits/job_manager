package com.restfullapi.rest_full.requests.job;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobRequest {
    @NotBlank(message = "Title is required")
    private String title;

    @NotNull(message = "KPI is required")
    private int kpi;

    @NotNull(message = "Priority is required")
    private int priority;

    @NotNull(message = "Status is required")
    private int status;

    @NotNull(message = "Is Task is required")
    private Boolean isTask;

    @NotNull(message = "Is staff is required")
    private String staff;

    @NotNull(message = "Is receiver is required")
    private String receiver;

    @Valid
    private JobDetailsRequest jobDetail;
}
package com.restfullapi.rest_full.requests.job;

import com.restfullapi.rest_full.containts.Message;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JobDetailsRequest {
    @Length(max = 500, message = Message.CANT_EXCEED_500_CHARACTERS)
    private String description;

    @Length(max = 255, message = Message.CANT_EXCEED_255_CHARACTERS)
    private String verifyLink;

    private int target;

    @Length(max = 255, message = Message.CANT_EXCEED_255_CHARACTERS)
    private String note;

    @Length(max = 255, message = Message.CANT_EXCEED_255_CHARACTERS)
    private String denyReason;

    private Timestamp timeStart;
    private Timestamp timeEnd;

    @Length(max = 255, message = Message.CANT_EXCEED_255_CHARACTERS)
    private String additionInfo;
}

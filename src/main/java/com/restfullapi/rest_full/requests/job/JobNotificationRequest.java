package com.restfullapi.rest_full.requests.job;

import lombok.Data;

@Data
public class JobNotificationRequest {
    private String content;
    private String from;
}

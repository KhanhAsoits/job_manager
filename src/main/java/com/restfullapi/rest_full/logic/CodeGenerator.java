package com.restfullapi.rest_full.logic;

import java.security.SecureRandom;

public class CodeGenerator {
    private static final int CODE_LENGTH = 6;
    private static final String CODE_CHARACTERS = "0123456789";

    private final SecureRandom secureRandom = new SecureRandom();

    public String generateRandomCode() {
        char[] code = new char[CODE_LENGTH];
        for (int i = 0; i < CODE_LENGTH; i++) {
            int randomIndex = secureRandom.nextInt(CODE_CHARACTERS.length());
            code[i] = CODE_CHARACTERS.charAt(randomIndex);
        }
        return new String(code);
    }
}
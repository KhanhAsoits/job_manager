package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Entity
@Data
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "role_name")
    @NotNull
    private String roleName;

    @Nullable
    private String description;

    @ManyToMany
    @JoinTable(
            name = "permissions_roles",
            joinColumns = @JoinColumn(name = "role_id"),
            inverseJoinColumns = @JoinColumn(name = "permission_id")
    )
    private List<Permission> permissions;

    @ManyToMany(mappedBy = "roles")
    @JsonIgnore
    private List<KPICategory> kpiCategories;

    public void addPerm(Permission permission) {
        this.permissions.add(permission);
    }

    public void removePerm(Permission permission) {
        this.permissions.remove(permission);
    }

    public boolean exitsPermissionById(Long permId) {
        List<Permission> oldPerm = this.permissions;
        for (Permission perm : oldPerm) {
            if (perm.getId() == permId) {
                return true;
            }
        }
        return false;
    }
}

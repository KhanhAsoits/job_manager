package com.restfullapi.rest_full.models;

import lombok.Data;

@Data
public class Notification {
    private String content;
}

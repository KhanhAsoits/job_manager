package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "users_kpis")
public class UserKpi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "result")
    private int result;

    @Column(name = "completion_status")
    private int completionStatus;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "kpi_id")
    private Kpi kpi;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private User receiver;

    @JsonIgnore
    @Column(name = "evaluate_from_reporter")
    private String evaluateFromReporter;
}

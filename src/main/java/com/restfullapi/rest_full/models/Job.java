package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Entity
@Data
@Table(name = "jobs")
public class Job {

    public Job() {
        this.id = UUID.randomUUID().toString();
    }

    @Id
    private String id;

    @Column(name = "title")
    @NotNull
    private String title;

    @Column(name = "kpi")
    @NotNull
    private int kpi;

    @Column(name = "priority")
    @NotNull
    private int priority;

    @Column(name = "status")
    @NotNull
    private int status;

    @Column(name = "is_task")
    @NotNull
    private boolean isTask;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "staff_id")
    private User staff;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private User receiver;

    @OneToOne(mappedBy = "job", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    @Nullable
    private JobDetail jobDetail;

    @JsonIgnore
    @OneToMany(mappedBy = "job", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Kpi> kpis;
}


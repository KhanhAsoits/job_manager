package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;

@Entity
@Data
@Table(name = "job_details")
public class JobDetail {
    @Id
    @Column(name = "job_id")
    private String id;

    @Column(name = "description", length = 500)
    private String description;

    @Column(name = "verify_link", length = 225)
    private String verifyLink;

    @Column(name = "target")
    private int target;

    @Column(name = "note", length = 255)
    private String note;

    @Column(name = "deny_reason", length = 225)
    private String denyReason;

    @Column(name = "time_start", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp timeStart;

    @Column(name = "time_end", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp timeEnd;

    @Column(name = "addition_info", length = 255)
    private String additionInfo;

    @OneToOne
    @JoinColumn(name = "job_id")
    @MapsId
    @JsonIgnore
    private Job job;
}

package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;

@Entity
@Table(name = "kpi_list")
@Data
public class KPICategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "kpi_name")
    private String name;

    @Nullable
    private String description;

    @ManyToMany
    @JoinTable(
            name = "kpis_roles",
            joinColumns = @JoinColumn(name = "kpi_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private List<Role> roles;

    @OneToMany(mappedBy = "kpiCategory", cascade = CascadeType.ALL)
    @JsonIgnore
    private List<Kpi> kpis;

    public void addRole(Role role) {
        this.roles.add(role);
    }

    public void removeRole(Role role) {
        this.roles.remove(role);
    }

    public boolean roleWasExisted(Long roleId) {
        for (Role role: this.roles) {
            if (role.getId() == roleId) {
                return true;
            }
        }
        return false;
    }
}

package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "plan_details")
public class PlanDetail {
    public PlanDetail() {
        this.id = UUID.randomUUID().toString();
    }
    @Id
    @Column(name = "id", length = 50)
    private String id;

    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "plan_type")
    private int planType;

    @Column(name = "note", length = 1000)
    private String note;

    @Column(name = "time_start", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp timeStart;

    @Column(name = "time_end", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp timeEnd;

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "plan_id")
    private Plan plan;
}

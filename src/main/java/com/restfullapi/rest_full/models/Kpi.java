package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Entity
@Data
@Table(name = "kpis")
public class Kpi {
    public Kpi() {
        this.id = UUID.randomUUID().toString();
    }
    @Id
    @Column(name = "id", length = 50)
    private String id;

    @Column(name = "kpi_name", unique = true)
    @NotNull
    private String kpiName;

    @Column(name = "description", length = 1000)
    private String description;

    @Column(name = "target", nullable = false)
    private int target;

    @ManyToOne
    @JoinColumn(name = "kpi_type_id")
    private KPICategory kpiCategory;

    @ManyToOne
    @JoinColumn(name = "job_id")
    private Job job;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "kpi", cascade = CascadeType.ALL)
    private List<UserKpi> userKpis;
}

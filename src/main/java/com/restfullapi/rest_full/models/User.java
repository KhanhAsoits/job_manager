package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.validator.constraints.Length;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@Table(name = "users")
public class User {
    public User() {
        this.id = UUID.randomUUID().toString();
    }

    @Id
    private String id;

    @Column(name = "email", unique = true)
    @NotNull
    private String email;

    @Column(name = "full_name")
    @NotNull
    private String fullName;

    @Column(name = "phong_ban")
    @NotNull
    private String phong_ban;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "phone", unique = true)
    @NotNull
    @Length(max = 12, min = 10)
    private String phone;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    @Column(name = "updated_at")
    @UpdateTimestamp
    private Date updatedAt;

    @JsonIgnore
    @OneToMany(mappedBy = "receiver")
    private List<Job> receiverJobs;

    @JsonIgnore
    @OneToMany(mappedBy = "staff")
    private List<Job> jobs;

    @JsonIgnore
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Account account;

    @JsonIgnore
    @OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<UserKpi> userKpis;

    @JsonIgnore
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Kpi> kpis;

    @JsonIgnore
    @OneToMany(mappedBy = "receiver", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Plan> plans;

}

package com.restfullapi.rest_full.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Data
@Table(name = "kpi_details")
public class KpiDetail {

    public KpiDetail() {
        this.id = UUID.randomUUID().toString();
    }

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "note", length = 1000)
    private String note;

    @Column(name = "comment", length = 1000)
    private String comment;

    @Column(name = "time_comment")
    private LocalDateTime timeComment;

    @Column(name = "verify", length = 1000)
    private String verify;

    @Column(name = "time_start", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp timeStart;

    @Column(name = "time_end", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Timestamp timeEnd;
}

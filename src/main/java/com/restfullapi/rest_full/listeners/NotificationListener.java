package com.restfullapi.rest_full.listeners;

import com.restfullapi.rest_full.events.JobNotificationEvent;
import com.restfullapi.rest_full.requests.job.JobNotificationRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NotificationListener {

    private final SimpMessageSendingOperations msgTemplete;

    @EventListener
    public void notificationListener(JobNotificationEvent notificationEvent) {
        JobNotificationRequest notification = new JobNotificationRequest();
        msgTemplete.convertAndSend("/job/notification", notification);
    }
}

package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.apis.interfaces.ICrudAPIController;
import com.restfullapi.rest_full.enums.Permissions;
import com.restfullapi.rest_full.exceptions.NoPermissionException;
import com.restfullapi.rest_full.requests.kpiCategory.GiveRoleForKPICategoryRequest;
import com.restfullapi.rest_full.requests.role.GivePermissionForRoleRequest;
import com.restfullapi.rest_full.requests.role.RoleRequest;
import com.restfullapi.rest_full.services.kpiCategory.KPICategoryService;
import com.restfullapi.rest_full.services.role.RoleService;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/roles")
@CrossOrigin
@RestController
@RequiredArgsConstructor
public class RoleApi implements ICrudAPIController<RoleRequest, Long> {
    private final ProcessPermission processPermission;
    private final RoleService roleService;
    private final KPICategoryService kpiCategoryService;

    @GetMapping()
    @Override
    public ResponseEntity<?> getAll(Pageable pageable) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ROLE_READ);
            return roleService.s_getAll(pageable);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PutMapping("/give_permission_for_role/{id}")
    public ResponseEntity<?> givePermissionForRole(@Valid @RequestBody GivePermissionForRoleRequest givePermissionForRoleRequest,
                                                   @PathVariable("id") Long id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ROLE_UPDATE);
            return roleService.s_givePermissionForRole(givePermissionForRoleRequest, id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PutMapping("/remove_permission_from_role/{id}")
    public ResponseEntity<?> removePermissionForRole(@Valid @RequestBody GivePermissionForRoleRequest givePermissionForRoleRequest,
                                                     @PathVariable("id") Long id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ROLE_UPDATE);
            return roleService.s_removePermissionForRole(givePermissionForRoleRequest, id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PutMapping("/give_role_for_kpi_category/{id}")
    public ResponseEntity<?> giveRoleForKPICategory(@Valid @RequestBody GiveRoleForKPICategoryRequest request,
                                                    @PathVariable("id") Long id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_UPDATE);
            return kpiCategoryService.s_giveRoleForKPICategory(request, id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PutMapping("/remove_role_for_kpi_category/{id}")
    public ResponseEntity<?> removeRoleForKPICategory(@Valid @RequestBody GiveRoleForKPICategoryRequest request,
                                                      @PathVariable("id") Long id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_UPDATE);
            return kpiCategoryService.s_removeRoleForKPICategory(request, id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @GetMapping("/{id}")
    @Override
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ROLE_READ);
            return roleService.s_getById(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PostMapping()
    @Override
    public ResponseEntity<?> store(@Valid @RequestBody RoleRequest roleRequest) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ROLE_CREATE);
            return roleService.s_store(roleRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PutMapping("/{id}")
    @Override
    public ResponseEntity<?> update(@PathVariable("id") Long id, @Valid @RequestBody RoleRequest roleRequest) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ROLE_UPDATE);
            return roleService.s_update(id, roleRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @DeleteMapping("/{id}")
    @Override
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ROLE_DELETE);
            return roleService.s_delete(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }
}

package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.apis.interfaces.ICrudAPIController;
import com.restfullapi.rest_full.enums.Permissions;
import com.restfullapi.rest_full.exceptions.NoPermissionException;
import com.restfullapi.rest_full.requests.job.JobRequest;
import com.restfullapi.rest_full.services.job.JobService;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequiredArgsConstructor
@RequestMapping("/api/jobs")
public class JobApi implements ICrudAPIController<JobRequest, String> {
    private final ProcessPermission processPermission;
    private final JobService jobService;

    @GetMapping()
    @Override
    public ResponseEntity<?> getAll(Pageable pageable) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_JOB_READ);
            return jobService.s_getAll(pageable);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PostMapping()
    @Override
    public ResponseEntity<?> store(@Valid @RequestBody(required = false) JobRequest jobRequest) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_JOB_CREATE);
            return jobService.s_store(jobRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PutMapping("/{id}")
    @Override
    public ResponseEntity<?> update(@PathVariable(name = "id") String id, @Valid @RequestBody JobRequest jobRequest) {
        try {
            System.out.println(id);
            processPermission.checkPermission(Permissions.MANAGE_JOB_UPDATE);
            return jobService.s_update(id, jobRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @DeleteMapping("/{id}")
    @Override
    public ResponseEntity<?> delete(@PathVariable(name = "id") String id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_JOB_DELETE);
            return jobService.s_delete(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @GetMapping("/{id}")
    @Override
    public ResponseEntity<?> getById(@PathVariable(name = "id") String id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_JOB_READ);
            return jobService.s_getById(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }
}

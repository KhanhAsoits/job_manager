package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.containts.Authenticaion;
import com.restfullapi.rest_full.enums.Permissions;
import com.restfullapi.rest_full.enums.Roles;
import com.restfullapi.rest_full.exceptions.NoPermissionException;
import com.restfullapi.rest_full.models.CustomUserDetail;
import com.restfullapi.rest_full.models.Permission;
import com.restfullapi.rest_full.models.Role;
import com.restfullapi.rest_full.repositories.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Component
@RequiredArgsConstructor
public class ProcessPermission {
    private final RoleRepository roleRepository;

    private final Logger logger = LoggerFactory.getLogger(ProcessPermission.class);

    public NoPermissionException getException() {
        return new NoPermissionException();
    }

    public void checkPermission(Permissions permissions) throws NoPermissionException {
        if (Objects.equals(Authenticaion.ANONYMOUS_USER_KEY, SecurityContextHolder.getContext().getAuthentication().getPrincipal())) {
            throw getException();
        }
        CustomUserDetail customUserDetail = (CustomUserDetail) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Role role = roleRepository.findByRoleName(customUserDetail.getAuthorities().stream().toList().get(0).getAuthority());
        List<Permission> userPermission = role.getPermissions();
        if (role.getRoleName().equals(String.valueOf(Roles.ADMIN))) {
            return;
        }
        for (Permission permission : userPermission) {
            if (Objects.equals(permission.getPermissionName(), String.valueOf(permissions))) {
                return;
            }
        }
        throw getException();
    }
}

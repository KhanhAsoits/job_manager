package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.apis.interfaces.ICrudAPIController;
import com.restfullapi.rest_full.enums.Permissions;
import com.restfullapi.rest_full.exceptions.NoPermissionException;
import com.restfullapi.rest_full.requests.kpiCategory.KPICategoryRequest;
import com.restfullapi.rest_full.services.kpiCategory.KPICategoryService;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/kpi_categories")
@RequiredArgsConstructor
public class KPICategoryApi implements ICrudAPIController<KPICategoryRequest, Long> {
    private final ProcessPermission processPermission;
    private final KPICategoryService kpiCategoryService;

    @GetMapping
    @Override
    public ResponseEntity<?> getAll(Pageable pageable) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_READ);
            return kpiCategoryService.s_getAll(pageable);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PostMapping
    @Override
    public ResponseEntity<?> store(@Valid @RequestBody KPICategoryRequest kpiCategoryRequest) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_CREATE);
            return kpiCategoryService.s_store(kpiCategoryRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PutMapping("/{id}")
    @Override
    public ResponseEntity<?> update(@PathVariable("id") Long id,
                                    @Valid @RequestBody KPICategoryRequest kpiCategoryRequest) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_UPDATE);
            return kpiCategoryService.s_update(id, kpiCategoryRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @DeleteMapping("/{id}")
    @Override
    public ResponseEntity<?> delete(@PathVariable("id") Long id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_DELETE);
            return kpiCategoryService.s_delete(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @GetMapping("/{id}")
    @Override
    public ResponseEntity<?> getById(@PathVariable("id") Long id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_READ);
            return kpiCategoryService.s_getById(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }
}

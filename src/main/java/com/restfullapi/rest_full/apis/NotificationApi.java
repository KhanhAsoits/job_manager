package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.requests.job.JobNotificationRequest;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class NotificationApi {

    @MessageMapping("/push.notification")
    @SendTo("/job/notifications")
    public JobNotificationRequest send(@Payload JobNotificationRequest notification) {
        return notification;
    }
}

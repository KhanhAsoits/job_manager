package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.apis.interfaces.ICrudAPIController;
import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.models.Account;
import com.restfullapi.rest_full.repositories.AccountRepository;
import com.restfullapi.rest_full.requests.account.AccountRequest;
import com.restfullapi.rest_full.requests.job.JobRequest;
import com.restfullapi.rest_full.responses.AccountResponse;
import com.restfullapi.rest_full.services.job.JobService;
import com.restfullapi.rest_full.services.staff.StaffService;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/staff")
public class StaffApi implements ICrudAPIController<AccountRequest, String> {
    private final StaffService staffService;

    @GetMapping()
    @Override
    public ResponseEntity<?> getAll(Pageable pageable) {
        try {
            return staffService.s_getAll(pageable);
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @Override
    public ResponseEntity<?> store(AccountRequest accountRequest) {
        return null;
    }

    @Override
    public ResponseEntity<?> update(String id, AccountRequest accountRequest) {
        return null;
    }

    @Override
    public ResponseEntity<?> delete(String id) {
        return null;
    }

    @Override
    public ResponseEntity<?> getById(String id) {
        return null;
    }
}

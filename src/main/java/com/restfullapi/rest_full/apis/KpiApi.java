package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.apis.interfaces.ICrudAPIController;
import com.restfullapi.rest_full.enums.Permissions;
import com.restfullapi.rest_full.exceptions.NoPermissionException;
import com.restfullapi.rest_full.requests.kpi.KpiRequest;
import com.restfullapi.rest_full.services.kpi.KpiService;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/kpis")
@CrossOrigin
@RequiredArgsConstructor
public class KpiApi implements ICrudAPIController<KpiRequest, String> {
    private final ProcessPermission processPermission;
    private final KpiService kpiService;

    @GetMapping
    @Override
    public ResponseEntity<?> getAll(Pageable pageable) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_READ);
            return kpiService.s_getAll(pageable);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PostMapping
    @Override
    public ResponseEntity<?> store(@Valid @RequestBody KpiRequest kpiRequest) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_CREATE);
            return kpiService.s_store(kpiRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @PutMapping("/{id}")
    @Override
    public ResponseEntity<?> update(@PathVariable("id") String id,
                                    @Valid @RequestBody KpiRequest kpiRequest) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_UPDATE);
            return kpiService.s_update(id, kpiRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @DeleteMapping("/{id}")
    @Override
    public ResponseEntity<?> delete(@PathVariable("id") String id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_DELETE);
            return kpiService.s_delete(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @GetMapping("/{id}")
    @Override
    public ResponseEntity<?> getById(@PathVariable("id") String id) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_KPI_READ);
            return kpiService.s_getById(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }
}

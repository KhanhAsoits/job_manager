package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.apis.interfaces.ICrudAPIController;
import com.restfullapi.rest_full.containts.Message;
import com.restfullapi.rest_full.enums.Permissions;
import com.restfullapi.rest_full.exceptions.NoPermissionException;
import com.restfullapi.rest_full.requests.RegisterRequest;
import com.restfullapi.rest_full.requests.account.AccountRequest;
import com.restfullapi.rest_full.services.account.AccountService;
import com.restfullapi.rest_full.services.auth.AuthService;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@CrossOrigin
@RequestMapping(path = "/api/account")
public class AccountApi implements ICrudAPIController<AccountRequest, String> {
    private final AuthService authService;
    private final AccountService accountService;
    private final ProcessPermission processPermission;

    @PostMapping("/create")
    public ResponseEntity<?> createAnAccount(@RequestBody(required = false) RegisterRequest registerRequest) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ACCOUNT_CREATE);
            return ApiResponseHelper.gI().resp(authService.register(registerRequest), HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @Override
    @GetMapping("/getUser")
    public ResponseEntity<?> getAll(Pageable pageable) {
        try {
            return accountService.s_getAll(pageable);
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @Override
    @PostMapping()
    public ResponseEntity<?> store(@Valid @RequestBody(required = false) AccountRequest accountRequest) {
        try {
            return accountService.s_store(accountRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable(name = "id") String id, @Valid @RequestBody AccountRequest accountRequest) {
        try {
            System.out.println(id);
            return accountService.s_update(id, accountRequest);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @Override
    @PutMapping("/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable(name = "id") String id) {
        try {
            return accountService.s_delete(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @Override
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable(name = "id") String id) {
        try {
            return accountService.s_getById(id);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @GetMapping("")
    public ResponseEntity<?> getAll() {
        try {
            processPermission.checkPermission(Permissions.MANAGE_ACCOUNT_READ);
            return ApiResponseHelper.gI().resp(authService.getAllAccount(), HttpStatus.OK, Message.DEFAULT_SUCCESS_MESSAGE);
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }
}
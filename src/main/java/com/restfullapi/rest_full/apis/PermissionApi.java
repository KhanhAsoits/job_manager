package com.restfullapi.rest_full.apis;

import com.restfullapi.rest_full.apis.interfaces.ICrudAPIController;
import com.restfullapi.rest_full.enums.Permissions;
import com.restfullapi.rest_full.exceptions.NoPermissionException;
import com.restfullapi.rest_full.requests.permission.PermissionRequest;
import com.restfullapi.rest_full.services.permission.PermissionService;
import com.restfullapi.rest_full.ultis.ApiResponseHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api/permissions")
@RequiredArgsConstructor
public class PermissionApi implements ICrudAPIController<PermissionRequest, Long> {
    private final PermissionService permissionService;
    private final ProcessPermission processPermission;

    @GetMapping
    @Override
    public ResponseEntity<?> getAll(Pageable pageable) {
        try {
            processPermission.checkPermission(Permissions.MANAGE_PERMISSION_READ);
            return permissionService.s_getAll(pageable);
        } catch (NoPermissionException e) {
            return ApiResponseHelper.gI().noPermission();
        } catch (Exception e) {
            return ApiResponseHelper.gI().fallback(e);
        }
    }

    @Override
    public ResponseEntity<?> store(PermissionRequest permissionRequest) {
        return null;
    }

    @Override
    public ResponseEntity<?> update(Long id, PermissionRequest permissionRequest) {
        return null;
    }

    @Override
    public ResponseEntity<?> delete(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<?> getById(Long id) {
        return null;
    }
}

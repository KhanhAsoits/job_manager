package com.restfullapi.rest_full.apis.interfaces;

import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface ICrudAPIController<Request, IdType> {
    public ResponseEntity<?> getAll(Pageable pageable);

    public ResponseEntity<?> store(@RequestBody Request request);

    public ResponseEntity<?> update(IdType id, @RequestBody Request request);

    public ResponseEntity<?> delete(IdType id);

    public ResponseEntity<?> getById(IdType id);
}

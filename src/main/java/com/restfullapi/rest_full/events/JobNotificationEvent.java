package com.restfullapi.rest_full.events;

import org.springframework.context.ApplicationEvent;

public class JobNotificationEvent extends ApplicationEvent {

    public JobNotificationEvent(Object source) {
        super(source);
    }
}

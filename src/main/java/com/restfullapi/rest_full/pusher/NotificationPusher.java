package com.restfullapi.rest_full.pusher;

import com.restfullapi.rest_full.events.JobNotificationEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class NotificationPusher {
    private final ApplicationEventPublisher applicationEventPublisher;

    public void pushJobNotification() {
        applicationEventPublisher.publishEvent(new JobNotificationEvent(this));
    }
}

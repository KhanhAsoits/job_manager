package com.restfullapi.rest_full.containts;

public class Message {
    public static final String DEFAULT_SUCCESS_MESSAGE = "Success";
    public static final String PASSWORD_INCORRECT = "Password incorrect";
    public static final String USER_NOT_FOUND = "User not found";
    public static final String NO_PERMISSION = "No permission for this action";
    public static final String VALUE_EXISTED = "This value was existed";
    public static final String REQUIRED = "This field required";
    public static final String NOT_FOUND = "Not found";
    public static final String ROLE_NOT_FOUND = "Role not found by id %d";
    public static final String NO_DATA = "No data";
    public static final String PERMISSION_NOT_FOUND = "Permission not found by id %d";
    public static final String TOKEN_INVALID = "Authentication token invalid";
    public static final String TOKEN_EXPIRED = "Authentication token expired";
    public static final String DEFAULT_ERROR_MESSAGE = "Error message";
    public static final String SYSTEM_ERROR_MESSAGE = "System error, please try again later";
    public static final String BAD_REQUEST = "Bad request";
    public static final String KPI_CATEGORY_NOT_FOUND = "KPI category not found by id %d";
    public static final String CANT_EXCEED_255_CHARACTERS = "This field cannot exceed 255 characters";
    public static final String CANT_EXCEED_500_CHARACTERS = "This field cannot exceed 500 characters";
    public static final String CANT_EXCEED_1000_CHARACTERS = "This field cannot exceed 1000 characters";
    public static final String JOB_NOTE_FOUND = "Job not found by id %s";
    public static final String KPI_NOT_FOUND = "Kpi not found by id %s";
    public static final String INVALID_RESET_CODE = "Invalid reset code";
    public static final String EXPIRED_RESET_CODE = "Expired reset code";
    public static final String ACCOUNT_NOT_FOUND = "Account not found";
}

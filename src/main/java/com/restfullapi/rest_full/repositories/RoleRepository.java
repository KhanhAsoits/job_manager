package com.restfullapi.rest_full.repositories;

import com.restfullapi.rest_full.models.Role;
import com.restfullapi.rest_full.repositories.customs.CustomizedRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>, CustomizedRepository<Role> {
    Role findByRoleName(String name);
}

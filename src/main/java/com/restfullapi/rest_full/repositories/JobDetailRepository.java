package com.restfullapi.rest_full.repositories;

import com.restfullapi.rest_full.models.JobDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobDetailRepository extends JpaRepository<JobDetail, String> {
}

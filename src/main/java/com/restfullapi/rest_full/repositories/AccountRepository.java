package com.restfullapi.rest_full.repositories;

import com.restfullapi.rest_full.models.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByUsername(String username);
    @Query(value = "select * from accounts where role_id = :roleId", nativeQuery = true)
    List<Account> findAllByRoleId(Long roleId);
    @Query(value = "select * from accounts where role_id != :roleId", nativeQuery = true)
    List<Account> findAllIfNotRole(Long roleId);
}

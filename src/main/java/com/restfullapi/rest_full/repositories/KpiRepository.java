package com.restfullapi.rest_full.repositories;

import com.restfullapi.rest_full.models.Kpi;
import com.restfullapi.rest_full.repositories.customs.CustomizedRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KpiRepository extends JpaRepository<Kpi, String>, CustomizedRepository<Kpi> {
}

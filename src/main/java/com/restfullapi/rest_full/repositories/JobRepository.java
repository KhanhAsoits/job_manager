package com.restfullapi.rest_full.repositories;

import com.restfullapi.rest_full.models.Job;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface JobRepository extends JpaRepository<Job, String> {

    @Query(value = "select * from jobs where receiver_id = :userId",nativeQuery = true)
    List<Job> findAllByUser(Pageable pageable, String userId);
}

package com.restfullapi.rest_full.repositories;

import com.restfullapi.rest_full.models.KPICategory;
import com.restfullapi.rest_full.repositories.customs.CustomizedRepository;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KPICategoryRepository extends JpaRepository<KPICategory, Long>, CustomizedRepository<KPICategory> {
}

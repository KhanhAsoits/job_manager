package com.restfullapi.rest_full.repositories.customs;

public interface CustomizedRepository<T> {
    boolean valueWasExisted(String columnName, Object value, Class<T> entityType);
}

package com.restfullapi.rest_full.repositories.customs;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomizedRepositoryImpl<T> implements CustomizedRepository<T> {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public boolean valueWasExisted(String columnName, Object value, Class<T> entityType) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = builder.createQuery(entityType);
        Root<T> root = query.from(entityType);

        Predicate condition = builder.equal(root.get(columnName), String.valueOf(value));

        query.select(root).where(condition);

        try {
            List<T> resultList = entityManager.createQuery(query).getResultList();
            return !resultList.isEmpty();
        } catch (Exception e) {
            return false;
        }
    }
}
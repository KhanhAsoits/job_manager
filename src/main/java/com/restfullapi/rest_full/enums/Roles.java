package com.restfullapi.rest_full.enums;

public enum Roles {
    ADMIN,
    TEACHER,
    MANAGER,
    STAFF
}
